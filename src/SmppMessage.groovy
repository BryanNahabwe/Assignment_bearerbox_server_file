/**
 * Created by Bryn on 9/9/2016.
 */
class SmppMessage {
    //Method for checking the if the regular expression matches from each line of the file
    public static String processEachLine(String regularExp, String pattern, int eachMatch, int group) {
        def matched = pattern =~ /$regularExp/
            if (matched) {
                return matched[eachMatch][group]
            }
    }
        public static void main(String[] args) {
            String smpp = new File('G:/Omni/bearerbox_server.txt').text;
             InstanceVariables messReq = new InstanceVariables();
             InstanceVariables messResp = new InstanceVariables();
             List<InstanceVariables> reqList = [];
            // List<InstanceVariables> respList = [];
            //mapping requests and responses
            String[] request = ["type_name: submit_sm", "type_name: deliver_sm"];
            String[] response = ["type_name: submit_sm_resp", "type_name: deliver_sm_resp"];
                smpp.eachLine { file ->
                    //Regular Expressions patterns
                    def datePattern = /(\d{4}-\d{2}-\d{2})/
                    def timePattern = /(\d{2}:\d{2}:\d{2})/
                    def startDumpPattern = /(SMPP\sPDU\s\d.(\d{3}.\d{3})\s\w+:)/
                    def endDumpPattern = /(SMPP\sPDU\sdump\sends.)/
                    def sourceAddrPattern = /(source_addr:\s("\d+"))/
                    def typeNamePattern = /(type_name:)(\s)(\w*)/
                    def destAddrPattern = /(destination_addr:\s("\d+"))/
                    def commandStatusPattern = /(command_status:)\s(\d)/
                    //def sequencePattern = /(sequence_number:\s\d+\s=\s\d.\d.*)/

                    //Checking if the regular expressions patterns match the lines in the file
                    def dateMatch = processEachLine(datePattern, file, 0, 1);
                    def timeMatch = processEachLine(timePattern, file, 0, 1);
                    def typeNameMatch = processEachLine(typeNamePattern, file, 0, 0);
                    def startDumpMatch = processEachLine(startDumpPattern, file, 0, 0);
                    def endDumpMatch = processEachLine(endDumpPattern, file, 0, 0);
                    def sourceAddrMatch = processEachLine(sourceAddrPattern, file, 0, 2);
                    def destAddrMatch = processEachLine(destAddrPattern, file, 0, 2);
                    def commandStatusMatch = processEachLine(commandStatusPattern, file, 0, 2);
                    //def sequenceMatch = processEachLine(sequencePattern, file, 0, 2);



                    //Conditions to sort out the data within the file according to what has been matched
                if (dateMatch && timeMatch && (typeNameMatch || sourceAddrMatch || destAddrMatch ||
                       commandStatusMatch || endDumpMatch || startDumpMatch)) {

                    if (typeNameMatch != null && (typeNameMatch.equalsIgnoreCase(request[0]) ||
                            typeNameMatch.equalsIgnoreCase(request[1]))) {
                        messReq = new InstanceVariables()

                    }
                    if (typeNameMatch != null && (typeNameMatch.equalsIgnoreCase(response[0]) ||
                            typeNameMatch.equalsIgnoreCase(response[1]))) {
                        messResp = new InstanceVariables();
                    }

                         messReq.date = dateMatch;
                         messReq.time = timeMatch;


                    if (destAddrMatch!=null) {
                        messReq.dest = destAddrMatch
                    }
                    if (sourceAddrMatch!=null) {
                        messReq.source = sourceAddrMatch
                    }
                    if (commandStatusMatch != null) {
                        messReq.command = commandStatusMatch

                    }

                    if (endDumpMatch != null) {
                        reqList << messReq

                    }
                }
                }


                 def output = new File('G:/Omni/sorted.txt');
                 output.withWriter('utf-8') { writer ->
                     writer.write("Date\t\t\tTime\t\t\tDestination\t\t\tSource\t\tCommand Status\n")
                     reqList.each { sms ->
                         writer.print(sms.date + "\t\t");
                         writer.print(sms.time + "\t\t");
                         writer.print(sms.dest + "\t\t\t\t");
                         writer.print(sms.source + "\t\t\t");
                         writer.print(sms.command + "\n");
                }
            }
     }
}

